import 'dart:async';
import 'dart:io' as io;
import 'package:aryasolution/entity/user_response.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class UserDb {
  static const int DB_VERSION = 1;
  static const String CONTACTS_SQL = """CREATE TABLE User( 
        id INTEGER PRIMARY KEY AUTOINCREMENT , 
        user_id TEXT ,
        first_name TEXT, 
        last_name TEXT, 
        email TEXT, 
        gender TEXT, 
        date_of_birth TEXT, 
        phone_no TEXT,
        company TEXT,
        title TEXT,
        image TEXT,
        retry INTEGER,
        favorite INTEGER)""";

  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "user_db.db");
    var theDb = await openDatabase(path,
        version: DB_VERSION, onCreate: _onCreate, onUpgrade: _onUpgrade);
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    await db.execute(CONTACTS_SQL);
  }

  void _onUpgrade(Database db, int oldVersion, int newVersion) async {
    return;
  }

  Future<List<User>> getUsers() async {
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM User');
    List<User> users = new List();
    for (int i = 0; i < list.length; i++) {
      users.add(User(
          id: list[i]["id"],
          userId: list[i]["user_id"],
          firstName: list[i]["first_name"],
          lastName: list[i]["last_name"],
          email: list[i]["email"],
          gender: list[i]["gender"],
          dateOfBirth: list[i]["date_of_birth"],
          phoneNo: list[i]["phone_no"],
          company: list[i]["company"],
          title: list[i]["title"],
          pathImage: list[i]["image"],
          retry: list[i]["retry"],
          isFavorite: list[i]["favorite"] > 0 ? true : false));
    }
    return users;
  }

  Future<User> getUser(String id) async {
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery(
        'SELECT * FROM User WHERE user_id = ?', [id]);
    if (list.length > 0) {
      return User(
          id: list[0]["id"],
          userId: list[0]["user_id"],
          firstName: list[0]["first_name"],
          lastName: list[0]["last_name"],
          email: list[0]["email"],
          gender: list[0]["gender"],
          dateOfBirth: list[0]["date_of_birth"],
          phoneNo: list[0]["phone_no"],
          company: list[0]["company"],
          title: list[0]["title"],
          pathImage: list[0]["image"],
          retry: list[0]["retry"],
          isFavorite: list[0]["favorite"] > 0 ? true : false);
    }
    return null;
  }

  Future<int> updateUser(User user) async {
    var dbClient = await db;
    return await dbClient.rawUpdate(
        'UPDATE User SET  first_name = ?'
            ', last_name = ?'
            ', email = ?'
            ', date_of_birth = ?'
            ', phone_no = ?'
            ', favorite = ?'
            ', company = ?'
            ', title = ?'
            ', image = ?'
            ', retry = ?'
            ', user_id = ?'
            ' WHERE id = ?',
        [
          user.firstName,
          user.lastName,
          user.email,
          user.dateOfBirth,
          user.phoneNo,
          user.isFavorite == true ? 1 : 0,
          user.company,
          user.title,
          user.pathImage,
          user.retry,
          user.userId,
          user.id
        ]);
  }

  Future<int> saveUser(User user) async {
    var dbClient = await db;
    return await dbClient.rawInsert(
        'INSERT OR REPLACE INTO User (user_id,first_name, last_name, email, date_of_birth, phone_no, gender, company, title, image, favorite, retry) '
            'values(?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
        [
          user.userId,
          user.firstName,
          user.lastName,
          user.email,
          user.dateOfBirth,
          user.phoneNo,
          user.gender.toString(),
          user.company,
          user.title,
          user.pathImage,
          user.isFavorite == true ? 1 : 0,
          user.retry
        ]);
  }

  Future<int> saveUsers(List<User> users) async {
    int count = 0;
    for (User c in users) {
      if (await saveUser(c) > 0) {
        count++;
      }
    }
    return count;
  }

  Future<bool> deleteUser(User user) async {
    var dbClient = await db;
    return await dbClient
        .rawDelete("DELETE FROM User WHERE id = ?", [user.id]) >
        0;
  }

  Future<bool> deleteUsers() async {
    var dbClient = await db;
    return await dbClient.rawDelete("DELETE FROM User") > 0;
  }
}
