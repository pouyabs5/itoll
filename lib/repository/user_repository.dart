import 'package:aryasolution/db/user_db.dart';
import 'package:aryasolution/entity/user_response.dart';
import 'package:aryasolution/net/api.dart';
import 'package:aryasolution/net/http_manager.dart';
import 'package:aryasolution/net/result_data.dart';
import 'package:dio/dio.dart';

class UserRepository {
  Future<List<User>> getUsers(int page, int row, {String userId}) async {
    List<User> userList = await getUsersDatabase();
    if (userList.length > 0) {
      return Future.value(userList);
    } else {
      String url =
          Api.getUsers(page, row, userId: userId != null ? userId : null);
      var res = await httpManager.netFetch(
          url, null, null, Options(method: HttpManager.GET));
      if (res != null && res.result) {
        if (UserResponse.fromJson(res.data).data.length > 0) {
          await saveToDatabase(UserResponse.fromJson(res.data).data);
          userList = await getUsersDatabase();
        }
        return Future.value(userList);
      } else if (res != null) {
        return Future.error(res);
      } else {
        return Future.error(ResultData("expectation error", false, -1));
      }
    }
  }

  Future<User> createUser(User user,
      {bool forceUpdateToDatabase = false}) async {
    String url = Api.createUser();
    var res = await httpManager.netFetch(
        url, user.toJson(), null, Options(method: HttpManager.POST));
    if (res != null && res.result) {
      if (forceUpdateToDatabase) {
        updateToDatabase(user
          ..userId = res.data["data"]["id"]
          ..retry = 0);
      } else {
        saveToDatabase(user
          ..userId = res.data["data"]["id"]
          ..retry = 0);
      }
      return Future.value(user);
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future<User> updateUser(User user) async {
    String url = Api.updateUser(user.userId);
    var res = await httpManager.netFetch(
        url, user.toJson(), null, Options(method: HttpManager.PUT));
    if (res != null && res.result) {
      updateToDatabase(user);
      return Future.value(user);
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future<UserResponse> deleteUser(User user) async {
    String url = Api.deleteUser(user.userId);
    var res = await httpManager.netFetch(
        url,
        null,
        null,
        Options(method: HttpManager.DELETE));
    if (res != null && res.result) {
      deleteFromDatabase(user);
      return Future.value(UserResponse.fromJson(res.data));
    } else if (res != null) {
      return Future.error(res);
    } else {
      return Future.error(ResultData("expectation error", false, -1));
    }
  }

  Future<List<User>> getUsersDatabase() {
    return UserDb().getUsers();
  }

  Future<User> getUserDatabase(String userId) {
    return UserDb().getUser(userId);
  }

  void updateToDatabase(User user) {
    UserDb().updateUser(user);
  }

  Future<void> saveToDatabase(Object user) async {
    if (user is List) {
      await UserDb().saveUsers(user);
    } else {
      UserDb().saveUser(user);
    }
  }

  void deleteFromDatabase(User user){
    UserDb().deleteUser(user);
  }

}
