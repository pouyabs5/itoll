import 'dart:convert';

UserResponse userFromJson(String str) =>
    UserResponse.fromJson(json.decode(str));

String userToJson(UserResponse data) => json.encode(data.toJson());

class UserResponse {
  UserResponse({
    this.data,
    this.page,
    this.row,
    this.totalRow,
  });

  List<User> data;
  int page;
  int row;
  int totalRow;

  factory UserResponse.fromJson(Map<String, dynamic> json) => UserResponse(
        data: json["data"] == null
            ? null
            : List<User>.from(json["data"].map((x) => User.fromJson(x))),
        page: json["page"] == null ? null : json["page"],
        row: json["row"] == null ? null : json["row"],
        totalRow: json["total_row"] == null ? null : json["total_row"],
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
        "page": page == null ? null : page,
        "row": row == null ? null : row,
        "total_row": totalRow == null ? null : totalRow,
      };
}

class User {
  User({
    this.id,
    this.userId,
    this.firstName,
    this.lastName,
    this.email,
    this.gender,
    this.dateOfBirth,
    this.phoneNo,
    this.isFavorite = false,
    this.title = "",
    this.pathImage,
    this.company = "",
    this.retry = 0,
  });
  int id;
  String userId;
  String firstName;
  String lastName;
  String email;
  String gender;
  String dateOfBirth;
  String phoneNo;
  String title;
  String company;
  String pathImage;
  int retry;
  bool isFavorite;

  factory User.fromJson(Map<String, dynamic> json) => User(
        userId: json["id"] == null ? null : json["id"],
        firstName: json["first_name"] == null ? "" : json["first_name"],
        lastName: json["last_name"] == null ? "" : json["last_name"],
        email: json["email"] == null ? null : json["email"],
        gender: json["gender"] == null ? null : json["gender"],
        dateOfBirth:
            json["date_of_birth"] == null ? null : json["date_of_birth"],
        phoneNo: json["phone_no"] == null ? null : json["phone_no"],
      );

  Map<String, dynamic> toJson() => {
        "id": userId == null ? null : userId,
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
        "email": email == null ? null : email,
        "gender": gender == null ? null : gender,
        "date_of_birth": dateOfBirth == null ? null : dateOfBirth,
        "phone_no": phoneNo == null ? null : phoneNo,
      };

  @override
  String toString() {
    return 'User{id: $id, userId: $userId, firstName: $firstName, lastName: $lastName, email: $email, gender: $gender, dateOfBirth: $dateOfBirth, phoneNo: $phoneNo, title: $title, company: $company, pathImage: $pathImage, retry: $retry, isFavorite: $isFavorite}';
  }
}
