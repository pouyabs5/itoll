import 'dart:io';
import 'dart:math';

import 'package:aryasolution/entity/edit_mode.dart';
import 'package:aryasolution/entity/user_response.dart';
import 'package:aryasolution/provider/contact_provider.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class EditContactScreen extends StatefulWidget {
  final EditMode _editMode;
  final User _user;

  EditContactScreen(this._editMode, this._user);

  @override
  _EditContactScreenState createState() => _EditContactScreenState();
}

class _EditContactScreenState extends State<EditContactScreen> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _titleController = TextEditingController();
  TextEditingController _companyController = TextEditingController();
  TextEditingController _mobileController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  ContactProvider _contactProvider;
  String _imagePath;

  Future getImage() async {
    await ImagePicker().getImage(source: ImageSource.gallery).then((image) {
      setState(() {
        _imagePath = image.path;
      });
    });
  }

  @override
  void initState() {
    _contactProvider = Provider.of<ContactProvider>(context, listen: false);
    if (widget._editMode == EditMode.MODE_EDIT) {
      _nameController.text = widget._user.firstName;
      _titleController.text = widget._user.title;
      _companyController.text = widget._user.company;
      _mobileController.text = widget._user.phoneNo;
      _lastNameController.text = widget._user.lastName;
      _imagePath = widget._user.pathImage;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            widget._editMode == EditMode.MODE_EDIT
                ? "Edit Contact"
                : "New Contact",
            style: TextStyle(
              color: Colors.black,
            ),
          ),
          elevation: 0,
          backgroundColor: Colors.white,
          centerTitle: true,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 200,
                height: 200,
                child: Stack(
                  children: [
                    ClipOval(
                      child: _imagePath == null
                          ? Image.asset(
                              "assets/contact_placeholder.jpg",
                              width: 200,
                              fit: BoxFit.cover,
                              height: 200,
                            )
                          : Image.file(
                              File(_imagePath),
                              width: 200,
                              height: 200,
                              fit: BoxFit.cover,
                            ),
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Container(
                        width: 80,
                        height: 80,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                          color: Colors.blueGrey,
                        ),
                        child: IconButton(
                          onPressed: () {
                            getImage();
                          },
                          icon: Icon(Icons.photo_camera),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30.0, right: 30, top: 50),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text(
                        "Name",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: TextField(
                        controller: _nameController,
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          hintText: "enter name",
                          contentPadding:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(6),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30.0, right: 30, top: 10),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text(
                        "Last Name",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: TextField(
                        controller: _lastNameController,
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          hintText: "Enter Last Name",
                          contentPadding:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(6),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30.0, right: 30, top: 10),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text(
                        "title",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: TextField(
                        controller: _titleController,
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          hintText: "enter title",
                          contentPadding:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(6),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30.0, right: 30, top: 10),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text(
                        "Company",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: TextField(
                        controller: _companyController,
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          hintText: "enter company",
                          contentPadding:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(6),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 30.0, vertical: 10),
                child: Divider(
                  color: Colors.black,
                  height: 2,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30.0, right: 30),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text(
                        "Mobile",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: TextField(
                        controller: _mobileController,
                        textAlign: TextAlign.left,
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          hintText: "enter mobile",
                          contentPadding:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                          border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(6),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: 100,
                height: 40,
                margin: EdgeInsets.symmetric(
                  vertical: 40,
                ),
                alignment: Alignment.center,
                child: OutlineButton(
                  onPressed: () {
                    if (widget._editMode == EditMode.MODE_NEW) {
                      _contactProvider.createUser(
                          User(
                            userId: Random().nextInt(10000).toString(),
                            firstName: _nameController.text,
                            lastName: _lastNameController.text,
                            phoneNo: _mobileController.text,
                            title: _titleController.text,
                            company: _companyController.text,
                            pathImage: _imagePath,
                          ), () {
                        Navigator.of(context).pop();
                      });
                    } else {
                      widget._user
                        ..firstName = _nameController.text
                        ..lastName = _lastNameController.text
                        ..phoneNo = _mobileController.text
                        ..title = _titleController.text
                        ..company = _companyController.text
                        ..pathImage = _imagePath;
                      _contactProvider.updateUser(widget._user, () {
                        Navigator.of(context).pop();
                      });
                    }
                  },
                  child: Text("save"),
                  borderSide: BorderSide(
                    color: Colors.black,
                    width: 2,
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
