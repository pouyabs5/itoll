import 'dart:async';

import 'package:aryasolution/entity/edit_mode.dart';
import 'package:aryasolution/entity/select_mode.dart';
import 'package:aryasolution/entity/user_response.dart';
import 'package:aryasolution/provider/contact_provider.dart';
import 'package:aryasolution/screens/edit_contact_screen.dart';
import 'package:aryasolution/widgets/bottom_filter_toggle.dart';
import 'package:aryasolution/widgets/contact_item.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';

import '../entity/edit_mode.dart';
import 'edit_contact_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ContactProvider _contactProvider;
  SelectMode _selectMode = SelectMode.MODE_ALL;
  String _filterText = "";

  Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    _contactProvider = Provider.of<ContactProvider>(context, listen: false);
    _contactProvider.getUsers();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    initConnectivity();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ContactProvider>(
      builder: (ctx, data, child) {
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                onChanged: (String text) {
                  setState(() {
                    _filterText = text;
                  });
                },
                decoration: InputDecoration(
                  hintText: "Search",
                  prefixIcon: Icon(Icons.search),
                  contentPadding: EdgeInsets.all(0),
                  border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(24),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Stack(children: [
                ReorderableListView(
                  onReorder: (int start, int current) {
                    _contactProvider.changePositionList(start, current);
                  },
                  children: data
                      .getUserListOrFilterList(_selectMode, filter: _filterText)
                      .map(
                        (user) =>
                        Slidable(
                          key: ValueKey(user),
                          actionPane: SlidableStrechActionPane(),
                          secondaryActions: [
                            Container(
                              child: IconButton(
                                icon: Icon(Icons.edit),
                                onPressed: () {
                                  navigateToEditOrAddContact(
                                      EditMode.MODE_EDIT, user);
                                },
                              ),
                            ),
                            Container(
                              child: IconButton(
                                icon: Icon(Icons.delete),
                                onPressed: () {
                                  _contactProvider.removeUser(user);
                                },
                              ),
                            ),
                          ],
                          child: Container(
                            color: Colors.white,
                            child: ContactItem(user),
                          ),
                        ),
                  )
                      .toList(),
                ),
                Container(
                  margin: EdgeInsets.only(right: 16),
                  width: double.infinity,
                  alignment: Alignment.bottomRight,
                  child: FloatingActionButton(
                    child: Icon(Icons.add),
                    onPressed: () {
                      navigateToEditOrAddContact(EditMode.MODE_NEW, null);
                    },
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  height: double.infinity,
                  child: Selector<ContactProvider, bool>(
                    builder: (BuildContext context, bool value, Widget child) {
                      return Visibility(
                        visible: _contactProvider.showLoading,
                        child: CircularProgressIndicator(),
                      );
                    }, selector: (ctx, provider) => provider.showLoading,
                  ),
                ),
              ]),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: BottomFilterToggle((value) {
                setState(() {
                  _selectMode = value;
                });
              }),
            ),
          ],
        );
      },
    );
  }

  void navigateToEditOrAddContact(EditMode mode, User user) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditContactScreen(mode, user),
      ),
    );
  }

  Future<void> initConnectivity() async {
    ConnectivityResult result;
    try {
      result = await _connectivity.checkConnectivity();
    } catch (e) {
      print(e.toString());
    }
    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        _contactProvider.syncDatabaseToServer();
        break;
      case ConnectivityResult.mobile:
        _contactProvider.syncDatabaseToServer();
        break;
      case ConnectivityResult.none:
        break;
      default:
        break;
    }
  }

  @override
  void dispose() {
    _connectivitySubscription?.cancel();
    super.dispose();
  }
}
