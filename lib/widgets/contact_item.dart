import 'dart:io';

import 'package:aryasolution/entity/user_response.dart';
import 'package:aryasolution/provider/contact_provider.dart';
import 'package:aryasolution/screens/contact_detail_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ContactItem extends StatefulWidget {
  final User _user;

  @override
  _ContactState createState() => _ContactState();

  ContactItem(this._user);
}

class _ContactState extends State<ContactItem> {
  ContactProvider _contactProvider;

  @override
  void initState() {
    _contactProvider = Provider.of<ContactProvider>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: ClipOval(
        child: widget._user.pathImage == null
            ? Image.asset(
                "assets/contact_placeholder.jpg",
                width: 50,
                fit: BoxFit.cover,
                height: 50,
              )
            : Image.file(
                File(widget._user.pathImage),
                width: 50,
                height: 50,
                fit: BoxFit.cover,
              ),
      ),
      title: Text(
        widget._user.firstName + " " + widget._user.lastName,
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w500,
          fontSize: 18,
        ),
      ),
      subtitle: Text(
        widget._user.phoneNo,
        style: TextStyle(
          color: Colors.black54,
          fontSize: 14,
        ),
      ),
      trailing: IconButton(
        icon: Icon(
          widget._user.isFavorite ? Icons.star : Icons.star_border,
        ),
        onPressed: () {
          _contactProvider.updateFavorite(widget._user);
        },
      ),
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) =>
                ContactDetailScreen(widget._user),
          ),
        );
      },
    );
  }
}
