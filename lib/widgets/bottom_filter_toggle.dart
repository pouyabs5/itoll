import 'package:aryasolution/entity/select_mode.dart';
import 'package:flutter/material.dart';

class BottomFilterToggle extends StatefulWidget {

  final ValueChanged<SelectMode> onPressed;
  BottomFilterToggle(this.onPressed);

  @override
  _BottomFilterToggleState createState() => _BottomFilterToggleState();
}

class _BottomFilterToggleState extends State<BottomFilterToggle> {
  var selections = [true, false];

  @override
  Widget build(BuildContext context) {
    return ToggleButtons(
      children: [
        SizedBox(
          width: 90,
          child: Center(
            child: Text("All"),
          ),
        ),
        SizedBox(
          width: 90,
          child: Center(
            child: Text("Favorites"),
          ),
        ),
      ],
      borderRadius: BorderRadius.all(Radius.circular(15)),
      onPressed: (index) {
        setState(() {
          for (int i = 0; i < selections.length; i++) {
            selections[i] = i == index;
          }
          if (index==0) {
            widget.onPressed(SelectMode.MODE_ALL);
          } else{
            widget.onPressed(SelectMode.MODE_FAVORITE);
          }
        });
      },
      selectedColor: Colors.white,
      fillColor: Colors.grey,
      isSelected: selections,
    );
  }
}
