import 'dart:convert';

import 'package:dio/dio.dart';

import '../code.dart';
import '../result_data.dart';
class ResponseInterceptors extends InterceptorsWrapper {
  Dio refreshDio = Dio();
  Dio previous;

  ResponseInterceptors(previous) {
    this.previous = previous;
  }

  @override
  onResponse(Response response) async {
    RequestOptions option = response.request;
    var value;
    try {
      var header = response.headers[Headers.contentTypeHeader];
      if ((header != null && header.toString().contains("text"))) {
        value = new ResultData(response.data, true, Code.SUCCESS);
      } else if (response.statusCode >= 200 && response.statusCode < 300) {
        value = new ResultData(response.data, true, Code.SUCCESS,
            headers: response.headers);
      }
    } catch (e) {
      print(e.toString() + option.path);
      try{
        if (response.data is ResultData) {
        }else{
          value = new ResultData(response.data, false, response.statusCode,
              headers: response.headers);
        }
      }catch(e){
        value = new ResultData(response.data, false, response.statusCode,
            headers: response.headers);
      }
    }
    return value;
  }

}
