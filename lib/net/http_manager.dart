
import 'package:dio/dio.dart';

import 'code.dart';
import 'interceptors/error_interceptor.dart';
import 'interceptors/response_interceptor.dart';
import 'result_data.dart';

final HttpManager httpManager = new HttpManager();

class HttpManager {
  static const CONTENT_TYPE_JSON = "application/json";
  static const CONTENT_TYPE_FORM = "application/x-www-form-urlencoded";

  static const POST = "post";
  static const GET = "get";
  static const PUT = "put";
  static const DELETE = "delete";

  Dio _dio = new Dio();

  Dio getDio() => _dio;


  HttpManager() {
    _dio.interceptors.add(ErrorInterceptors(_dio));
    _dio.interceptors.add(ResponseInterceptors(_dio));
    _dio.interceptors.add(LogInterceptor(responseBody: true,requestBody: true));
  }

  Future<ResultData> netFetch(
      url, params, Map<String, dynamic> header, Options option) async {
    Response response;
    try {
      response = await _dio.request(
        url,
        data: params,
        options: option,
      );
    } on DioError catch (e) {
      return resultError(e);
    }
    if (response.data is DioError) {
      return resultError(response.data);
    }
    return response.data;
  }

  ResultData resultError(DioError e) {
    Response errorResponse;
    if (e.response != null) {
      errorResponse = e.response;
    } else {
      errorResponse = new Response(statusCode: -666);
    }
    if (e.type == DioErrorType.CONNECT_TIMEOUT ||
        e.type == DioErrorType.RECEIVE_TIMEOUT) {
      errorResponse.statusCode = Code.NETWORK_TIMEOUT;
    }
    return new ResultData(
        Code.errorHandleFunction(
          errorResponse.statusCode,
          e.response != null ? e.message : "Failed host lookup",
        ),
        false,
        errorResponse.statusCode);
  }
}

