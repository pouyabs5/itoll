class Api {
  static const String host = "https://mock-rest-api-server.herokuapp.com/api/v1";

  static String getUsers(int page, int row, {String userId}) {
    return "$host/user${userId != null ? "/$userId" : ""}?page=$page&row=$row";
  }

  static String updateUser(String userId) {
    return "$host/user/$userId";
  }

  static String deleteUser(String userId) {
    return "$host/user/$userId";
  }

  static String createUser() {
    return "$host/user";
  }
}
