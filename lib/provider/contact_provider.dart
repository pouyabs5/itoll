import 'package:aryasolution/entity/select_mode.dart';
import 'package:aryasolution/entity/user_response.dart';
import 'package:aryasolution/repository/user_repository.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ContactProvider extends ChangeNotifier {
  UserRepository _userRepository;

  ContactProvider(UserRepository userRepository) {
    _userRepository = UserRepository();
    syncDatabaseToServer();
  }

  List<User> _userList = [];

  int _page = 1;
  int _row = 100;
  bool _hasLoadMore = true;

  bool get hasLoadMore => _hasLoadMore;

  bool _showLoading = false;
  get showLoading => _showLoading;

  void nextPage() {
    _page++;
  }

  void resetPage() {
    _page = 1;
  }

  void resetList() {
    _userList.clear();
  }

  List<User> getUserListOrFilterList(SelectMode selectMode,
      {String filter = ""}) {
    if (selectMode == SelectMode.MODE_ALL) {
      if (filter.isNotEmpty) {
        return _userList
            .where((element) => (element.firstName
                    .toLowerCase()
                    .contains(filter.toLowerCase()) ||
                element.lastName.toLowerCase().contains(filter.toLowerCase())))
            .toList();
      }
      return _userList;
    } else {
      if (filter.isNotEmpty) {
        return _userList
            .where((element) => (element.firstName
                    .toLowerCase()
                    .contains(filter.toLowerCase()) ||
                element.lastName.toLowerCase().contains(filter.toLowerCase())))
            .where((element) => element.isFavorite == true)
            .toList();
      }
      return _userList.where((element) => element.isFavorite == true).toList();
    }
  }

  void updateUser(User user,VoidCallback finish) {
    _userRepository.updateUser(user).then((value) {
      updateUserList(user);
      finish();
    }).catchError((error) {
      _userRepository.updateToDatabase(user..retry = 1);
      updateUserList(user);
      finish();
    });
  }

  void createUser(User user,VoidCallback finish) {
    _userRepository.createUser(user).then((value) async {
      addUserToList(user);
      finish();
    }).catchError((error) {
      _userRepository.saveToDatabase(user..retry = 1);
      addUserToList(user);
      finish();
    });
    notifyListeners();
  }

  void removeUser(User user) {
    _userRepository.deleteUser(user).then((value) {
      deleteUserList(user);
    }).catchError((error) {
      _userRepository.deleteFromDatabase(user);
      deleteUserList(user);
    });
  }

  void updateFavorite(User user) {
    _userRepository.updateToDatabase(user..isFavorite = !user.isFavorite);
    updateUserList(user);
  }

  void getUsers() {
    setLoading(true);
    _userRepository.getUsers(_page, _row).then((value) {
      setLoading(false);
      if (value.isNotEmpty) {
        _userList.addAll(value);
        notifyListeners();
        nextPage();
      } else {
        setLoadMore(false);
      }
    }).catchError((error) {
      setLoading(false);
      Fluttertoast.showToast(
          msg: error.toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    });
  }

  void addUserToList(User user)  async {
    int id = await getUserId(user.userId);
    _userList.add(user..id = id);
    notifyListeners();
  }

  Future<int> getUserId(String userId) async {
    User user = await _userRepository.getUserDatabase(userId);
    if (user != null) {
      return user.id;
    }
    return -1;
  }

  void updateUserList(User user) {
    _userList = _userList.map((e) {
      if (e.id == user.id) {
        e = user;
      }
      return e;
    }).toList();
    notifyListeners();
  }

  void deleteUserList(User user) {
    int index = _userList.indexOf(user);
    _userList.removeAt(index);
    notifyListeners();
  }

  void setLoadMore(bool loadMore) {
    _hasLoadMore = loadMore;
    notifyListeners();
  }

  void changePositionList(int oldIndex, int newIndex) {
    if (newIndex > oldIndex) {
      newIndex -= 1;
    }
    User user = _userList[oldIndex];
    _userList.removeAt(oldIndex);
    _userList.insert(newIndex, user);
    notifyListeners();
  }

  void syncDatabaseToServer() {
    _userRepository.getUsersDatabase().then((value) async {
      if (value.isNotEmpty) {
        List<User> list = value.where((element) => element.retry == 1).toList();
        if (list.isNotEmpty) {
          for (int i = 0; i < list.length; i++) {
            await _userRepository
                .createUser(list[i], forceUpdateToDatabase: true)
                .then((update) {
              updateUserList(update);
            }).catchError((error) {});
          }
        }
      }
    });
  }
  void setLoading(shouldShow) {
    _showLoading = shouldShow;
  }
}
